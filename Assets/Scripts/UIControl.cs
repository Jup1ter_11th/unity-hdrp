using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class UIControl : MonoBehaviour
{
    public UIDocument _HUD;
    public UIDocument _GameState;

    void Awake()
    {
        _GameState.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.E))
        {
            _GameState.enabled = true;
            _HUD.enabled = false;
            Time.timeScale = 0;
        }

        if (Input.GetKeyUp(KeyCode.Q))
        {
            _GameState.enabled = false;
            _HUD.enabled = true;
            Time.timeScale = 1;
        }
    }
}
