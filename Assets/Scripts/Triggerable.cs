using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Triggerable : MonoBehaviour
{
    public UnityEvent start;
    public UnityEvent onTriggerEnter;
    public UnityEvent onTriggerExit;
    private void Start()
    {
        if (start != null)
            start.Invoke();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (onTriggerEnter != null)
            onTriggerEnter.Invoke();
    }
    private void OnTriggerExit(Collider other)
    {
        if (onTriggerExit != null)
            onTriggerExit.Invoke();
    }
}
